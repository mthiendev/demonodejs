const fs = require('fs');
const addTask = (title, description) => {
  //get old tasks
  const tasks = getTasks();
  //check exits task
  const foundedTask = tasks.find((item) => {
    return item.title === title;
  });
  if (foundedTask) return;
  //add task

  const task = { title, description };
  tasks.push(task);

  //write file
  saveTask(tasks);
};
const removeTask = (title) => {
  const tasks = getTasks();
  const index = tasks.findIndex((item) => {
    return item.title === title;
  });
  if (index !== -1) {
    tasks.splice(index, 1);
  }
  saveTask(tasks);
};
const updateTask = (title, description) => {
  const tasks = getTasks();
  const index = tasks.findIndex((item) => {
    return item.title === title;
  });
  if (index !== -1) {
    tasks[index].description = description;
  }
  saveTask(tasks);
};
const listAllTasks = () => {
  const tasks = getTasks();
  tasks.forEach((item) => {
    console.log('Title: ', item.title);
    console.log('Description: ', item.description);
    console.log('-----------------------------');
  });
};

const saveTask = (tasks) => {
  const tasksJSON = JSON.stringify(tasks);
  fs.writeFileSync('tasks.json', tasksJSON);
};
const getTasks = () => {
  try {
    const tasksBuffer = fs.readFileSync('tasks.json');
    const tasksJSON = tasksBuffer.toString();
    return JSON.parse(tasksJSON);
  } catch (error) {
    return [];
  }
};
module.exports = {
  addTask,
  removeTask,
  updateTask,
  listAllTasks,
};
